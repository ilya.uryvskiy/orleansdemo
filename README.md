# OrleansDemo

### Струтктура
 - OrleansDemo.GrainInterfaces - проект с контрактами для Grain'ов
 - OrleansDemo.Grains - проект с самими Grain'ами
 - OrleansDemo.Silo - хостинг сервис для Grain, участник класстера Orleans, Silo
 - OrleansDemo.Client - демо клиент

### Ссылки
- [Документация по Orleans](https://dotnet.github.io/orleans/docs/index.html).
- [Скрипт для создания необходимых таблиц](https://github.com/dotnet/orleans/blob/main/src/AdoNet/Shared/PostgreSQL-Main.sql)
- [Скрипт для создания схемы Persistence для постгреса](https://github.com/dotnet/orleans/blob/main/src/AdoNet/Orleans.Persistence.AdoNet/PostgreSQL-Persistence.sql)


### Как запустить
Сначала запускаем OrleansDemo.Silo потом OrleansDemo.Client
#### OrleansDemo.Silo
- Проекте требует наличия постгреса, строка подключения захардкожена в классе Program `private const string ConnectionString = "Host=localhost;Database=OrleansDemo;Username=postgres;Password=1;";`
- Перед тем как запускать на базу нужно накатить скрипты для создания необходимых таблиц и для создания схемы Persistence
- Затем в корне проекта `dotnet run`

#### OrleansDemo.Client
- В корне проекта выполнить `dotnet run`

