﻿using System;
using System.Threading.Tasks;
using Orleans;

namespace OrleansDemo.GrainInterfaces
{
    public interface IIncident : IGrainWithGuidKey
    {
        Task<string> GetStatus();

        Task SetStatus(string status);
    }
}
