﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Providers;
using OrleansDemo.GrainInterfaces;

namespace OrleansDemo.Grains
{
    [StorageProvider(ProviderName = "OrleansDemo")]
    public class IncidentGrain : Grain<Incident>, IIncident
    {
        private readonly ILogger<IncidentGrain> _logger;

        public IncidentGrain(ILogger<IncidentGrain> logger)
        {
            _logger = logger;
        }

        public Task<string> GetStatus()
        {
            var grainId = this.GetPrimaryKey();
            var status = State.Status;
            _logger.LogInformation($"GetStatus. GrainId: {grainId}. Status {status}");
            return Task.FromResult(status);
        }

        public async Task SetStatus(string status)
        {
            var grainId = this.GetPrimaryKey();
            State.Status = status;
            await this.WriteStateAsync();
            _logger.LogInformation($"SetStatus. GrainId: {grainId}. Status {status}");
        }
    }

    [Serializable]
    public class Incident
    {
        public string Status { get; set; } = "None";
    }
}
