﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using OrleansDemo.GrainInterfaces;

namespace OrleansDemo.Client
{
    internal class Program
    {
        private static async Task<int> Main(string[] args)
        {
            try
            {
                using (var client = await ConnectClient())
                {
                    await DoClientWork(client);
                    Console.ReadKey();
                }

                return 0;
            }
            catch (Exception e)
            {
                Console.WriteLine($"\nException while trying to run client: {e.Message}");
                Console.WriteLine("Make sure the silo the client is trying to connect to is running.");
                Console.WriteLine("\nPress any key to exit.");
                Console.ReadKey();
                return -1;
            }
        }

        private static async Task<IClusterClient> ConnectClient()
        {
            IClusterClient client = new ClientBuilder()
                .UseLocalhostClustering()
                .Configure<ClusterOptions>(options =>
                {
                    options.ClusterId = "dev";
                    options.ServiceId = "OrleansBasics";
                })
                .ConfigureLogging(logging => logging.AddConsole())
                .Build();

            await client.Connect();
            Console.WriteLine("Client successfully connected to silo host");
            return client;
        }

        private static async Task DoClientWork(IClusterClient client)
        {
            // example of calling grains from the initialized client
            var incidentId = Guid.NewGuid();
            var incident = client.GetGrain<IIncident>(incidentId);

            var initialStatus = await incident.GetStatus();
            Console.WriteLine($"Initial status: {initialStatus}");

            var newStatus = "Open";
            await incident.SetStatus(newStatus);
            Console.WriteLine($"Set status to: {newStatus}");

            var currentStatus = await incident.GetStatus();
            Console.WriteLine($"Current sstatus: {currentStatus}");
        }
    }
}
