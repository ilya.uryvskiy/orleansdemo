﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using OrleansDemo.Grains;

namespace OrleansDemo.Silo
{
    internal class Program
    {
        private const string Invariant = "Npgsql"; // for Microsoft SQL Server
        private const string ConnectionString = "Host=localhost;Database=OrleansDemo;Username=postgres;Password=1;";

        private static async Task<int> Main(string[] args)
        {
            try
            {
                var host = await StartSilo();
                Console.WriteLine("\n\n Press Enter to terminate...\n\n");
                Console.ReadLine();

                await host.StopAsync();

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return -1;
            }
        }

        private static async Task<ISiloHost> StartSilo()
        {
            // define the cluster configuration
            var builder = new SiloHostBuilder()
                .UseLocalhostClustering()
                .Configure<ClusterOptions>(options =>
                {
                    options.ClusterId = "dev";
                    options.ServiceId = "OrleansBasics";
                })
                .ConfigureApplicationParts(parts => parts.AddApplicationPart(typeof(IncidentGrain).Assembly).WithReferences())
                .ConfigureLogging(logging => logging.AddConsole());

            //use AdoNet for Persistence
            builder.AddAdoNetGrainStorage("OrleansDemo", options =>
            {
                options.Invariant = Invariant;
                options.ConnectionString = ConnectionString;
            });

            var host = builder.Build();
            await host.StartAsync();
            return host;
        }
    }
}
